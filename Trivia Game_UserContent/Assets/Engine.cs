﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(References))]
public class Engine : MonoBehaviour
{
    References refs;
    int quesNumber;
    string word = "Starting Game";

    void Begin()
    {
        //gets a link to the references script
        refs = gameObject.GetComponent<References>();
        //start game
        StartGame();
    }

	void StartGame ()
    {
        int numSent = refs.questions.Count;
        int randomSent = Random.Range(0, numSent);

        word = refs.questions[randomSent];

        if (word.Length > 20)
        {
            return;
        }

        word = word.ToUpper();

        char[] delimiterChracters = { ' ' };
        string[] words = word.Split(delimiterChracters);

        //refs.score.text = points.ToString();
	}
}
