﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;


[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour
{
    FileInfo questionFile;
    FileInfo answerFile;
    TextAsset textFileQ;
    TextAsset textFileA;
    TextReader questionReader;
    TextReader answerReader;

    public List<string> questions = new List<string>();
    public List<string> answers = new List<string>();
    public List<string> wrongAnswers = new List<string>();

	// Use this for initialization
	void Start ()
    {
        questionFile = new FileInfo(Application.dataPath + "/UserQuestions.txt");
        answerFile = new FileInfo(Application.dataPath + "/UserAnswers.txt");

        if (questionFile != null && questionFile.Exists)
        {
            questionReader = questionFile.OpenText();
        }
        else
        {
            textFileQ = (TextAsset)Resources.Load("embeddedQuestions", typeof(TextAsset));
            questionReader = new StringReader(textFileQ.text);
        }//end Question File


        if (answerFile != null && answerFile.Exists)
        {
            answerReader = answerFile.OpenText();
        }
        else
        {
            textFileA = (TextAsset)Resources.Load("embeddedAnswers", typeof(TextAsset));
            answerReader = new StringReader(textFileA.text);
        }//end Answer File

        string questionLineOfText;
        string answerLineOfText;
        int questionsLineNum = 0;
        int answersLineNum = 0;

        //tell the reader variable to read a line of text, and store that in
        //the lineOfText variable. Continue until there are no lines left
        while((questionLineOfText = questionReader.ReadLine()) != null)
        {
            if (questionsLineNum % 1 == 0)
            {
                //will add to questions
                questions.Add(questionLineOfText);
            }

            questionsLineNum++;
        }

        while ((answerLineOfText = answerReader.ReadLine()) != null)
        {
            if (answersLineNum % 2 == 0)
            {
                //will add to the right answers
                answers.Add(answerLineOfText);
            }
            else
            {
                //will add to wrong answers
                wrongAnswers.Add(answerLineOfText);
            }
            answersLineNum++;
        }
        SendMessage("Gather");
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
