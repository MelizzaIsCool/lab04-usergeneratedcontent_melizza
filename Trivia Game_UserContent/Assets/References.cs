﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class References : MonoBehaviour
{
    [HideInInspector]
    public Text[] letters;
    [HideInInspector]
    public List<string> questions = new List<string>();
    [HideInInspector]
    public List<string> answers = new List<string>();

    void Gather()
    {
        questions = GetComponent<LoadScript>().questions;
        answers = GetComponent<LoadScript>().answers;

        letters = new Text[20];
    }
}
