﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;


[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour
{
    FileInfo orginalFile;
    TextAsset textFile;
    TextReader reader;

    public List<string> sentences = new List<string>();
    public List<string> clues = new List<string>();

	// Use this for initialization
	void Start ()
    {
        textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
        reader = new StringReader(textFile.text);

        orginalFile = new FileInfo(Application.dataPath + "/sentences.txt");
        if (orginalFile != null && orginalFile.Exists)
        {
            reader = orginalFile.OpenText();
        }
        else
        {
            textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
            reader = new StringReader(textFile.text);
        }

        string lineOfText;
        int lineNumber = 0;

        //tell the reader to read a line of text, and store that in the lineOfText variable
        //continue doing this until there are no lines left
        while ((lineOfText = reader.ReadLine()) != null)
        {
            if (lineNumber % 2 == 0)
            {
                //even lines
                sentences.Add(lineOfText);
            }
            else
            {
                //odd lines
                clues.Add(lineOfText);
            }

            lineNumber++;
        }
        SendMessage("Gather");
	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}


}
